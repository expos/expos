// GPS Location Sender
// David Pello 2013
// Plataforma0, LABoral centro de arte y creacion industrial
// For Maya de Rin 

#define  COM_SPEED  19200
#define  SEND_DELAY  1*1000

// variables
byte byteGPS = 0;
int i = 0;

// Buffers for data input
char nmea_sentence[100]="";
char latitude_string[15];
float latitude;
char north_south[2];
char longitude_string[15];
float longitude;
char east_west[2];
char altitude_string[10];
float altitude;
char fix[2];


//  convert 
float nmea_to_decimal(float nmea)
{
  int integer_part;
  float temp;
  
  // move decimal point
  temp = nmea/100;
  // get the decimal part
  integer_part = temp;
  temp = temp - integer_part;
  //convert from degrees
  temp = temp*100/60;
  
  // return the converted value
  return (float) integer_part + temp;
}
  

// get value from nmea string
void get_nmea_value(int number, char* dest)
{
  int count = 0;
  int index;
  char c;
  
  // advance until n separator 
  index = 0;
  while (count < number)
  {
    if (nmea_sentence[index] == ',')
      count++;
    index++;
  }
  
  //get value
  count = 0;
  while (nmea_sentence[index] != ',')
  {
    dest[count] = nmea_sentence[index];
    index++;
    count++;
  }
}


void setup()
{
  Serial.begin(COM_SPEED);
  // GPS warmup time
  delay(1000);
  // Configure GPS
  Serial2.begin(9600);
  //
  
  //Serial2.println("$PSTMGPSRESTART");
  //delay(100);
  Serial2.println("$PSTMINITGPS,4352.620,N,0056.330,W,0020,18,02,2013,10,30,00");
  Serial2.println("$PSTMNMEACONFIG,0,4800,1,1");
  
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);

}

void loop() 
{
  // Search for GPGGA sentence
  if (Serial2.find("GPGGA,"))
  {
    // found!, read until the end
    i=0;
    byteGPS=0;
    while (byteGPS!=13) {
      if (Serial2.available() > 0)
      {
        byteGPS=Serial2.read();
        nmea_sentence[i] = byteGPS;
        i++;
      }
    }
    nmea_sentence[i-1]=0;
    
    // Parse the sentence for values
    
    // Fix
    get_nmea_value(5, fix);
    if (fix[0] == '1')
      digitalWrite(13, HIGH);
    else
      digitalWrite(13, LOW);
    
    // Latitude
    get_nmea_value(1, latitude_string);
    if (latitude_string[0] != 0) 
    {
      latitude = atof(latitude_string);
      latitude = nmea_to_decimal(latitude);
    }
    else 
      latitude = 9999;
    
    get_nmea_value(2, north_south);
    if (north_south[0] == 'S')
      latitude = -latitude;
    
    // Longitude 
    get_nmea_value(3, longitude_string);
    if (longitude_string[0] != 0) 
    {
      longitude = atof(longitude_string);
      longitude = nmea_to_decimal(longitude);
    }
    else 
      longitude = 9999;
    
    get_nmea_value(4, east_west);
    if (east_west[0] == 'W')
      longitude = -longitude;
    
    
    // Altitude  
    get_nmea_value(8, altitude_string);
    if (altitude_string[0] != 0)
    {
      altitude = atof(altitude_string);
    }
    else
      altitude = -9999;
      
    Serial.print("XPOS,");
    Serial.print(latitude,5);
    Serial.print(",");
    Serial.print(longitude,5);
    Serial.print(",");
    Serial.print(altitude,2);
    Serial.println();
    
    delay(SEND_DELAY);
    
  }

}



