#include <AccelStepper.h>

// GPS Camera control
// David Pello 2013
// Plataforma0, LABoral centro de arte y creacion industrial
// For Maya de Rin 

#define CAM_ID  "CAM2"
#define COM_SPEED  19200

//#define BIG_EASY_DRIVER
//
//#ifdef BIG_EASY_DRIVER
//#define STEP_MULT  1
//#else
//#define STEP_MULT  2
//#endif

#define DIR_CORRECTION 1

#define UPDATE_BUTTON  10
#define UPDATE_LED     11
#define SATS_LED       12
#define FIX_LED        13

#define DIR_PIN 6
#define STEP_PIN 7

#define MAX_SPEED      900


AccelStepper stepper(AccelStepper::DRIVER, 7, 6);

// variables
byte byteGPS = 0;
byte byteXBEE = 0;
int i = 0;
double current_angle = 0;

int path1, path2;

// angle calculation
double dx, dy, ang = 0;
// distance
double distance;


// update position
int update;

// Buffers for NMEA data input
char nmea_sentence[100]="";
char latitude_string[15];
double latitude = 9999;
char north_south[2];
char longitude_string[15];
double longitude = 9999;
char east_west[2];
char altitude_string[10];
double altitude = -9999;
double temp_value;
char fix[2];
char sats[3];
int satn;

// buffers for GPS coordinates from xbee
char xbee_coordinates[50]="";
char xbee_latitude_string[15];
double xbee_latitude = -9999;
char xbee_longitude_string[15];
double xbee_longitude = -9999;
char xbee_altitude_string[15];
double xbee_altitude = -9999;


//  convert 
float nmea_to_decimal(float nmea)
{
  int integer_part;
  double temp;

  // move decimal point
  temp = nmea/100.0;
  // get the decimal part
  integer_part = (int)temp;
  temp = temp - integer_part;
  //convert from degrees
  temp = temp*100.0/60.0;

  // return the converted value
  return (double) integer_part + temp;
}


// get value from nmea string
void get_nmea_value(int number, char* dest)
{
  int count = 0;
  int index;
  char c;

  // advance until n separator 
  index = 0;
  while (count < number)
  {
    if (nmea_sentence[index] == ',')
      count++;
    index++;
  }

  //get value
  count = 0;
  while (nmea_sentence[index] != ',')
  {
    dest[count] = nmea_sentence[index];
    index++;
    count++;
  }
}

// get value from nmea string
void get_xbee_value(int number, char* dest)
{
  int count = 0;
  int index;
  char c;

  // advance until n separator 
  index = 0;
  while (count < number)
  {
    if (xbee_coordinates[index] == ',')
      count++;
    index++;
  }

  //get value
  count = 0;
  while (xbee_coordinates[index] != ',' && xbee_coordinates[index] != 0)
  {
    dest[count] = xbee_coordinates[index];
    index++;
    count++;
  }
}


double get_shortest_angle(double orig, double dest)
{
   double angle =  dest - orig;
   if (angle > 180)
     angle = angle - 360;
   if (angle < -180)
     angle = angle + 360;
     
   return angle;
}
    

void rotate_deg_accel(float deg)
{
  int dir = DIR_CORRECTION * ((deg>0)? 1:-1);
  //int steps = dir * (abs(deg)*(1/0.225)*43/10*STEP_MULT);
  int steps = dir * 6880.0*(abs(deg))/360.0; // 1/8 microstepping, 200 step motor and 10/43 gears -> 6880 steps per turn
  stepper.move(steps);
  while (abs(stepper.distanceToGo()) > 0)
      stepper.run();
}
  
void setup()
{
  Serial.begin(COM_SPEED);
  // GPS warmup time
  delay(1000);
  // Configure GPS
  Serial2.begin(4800);
  //

  //Serial2.println("$PSTMGPSRESTART");
  //delay(100);
  Serial2.println("$PSTMINITGPS,4352.620,N,0056.330,W,0020,18,02,2013,10,30,00");
  Serial2.println("$PSTMNMEACONFIG,0,4800,1,1");

  // fix
  pinMode(FIX_LED, OUTPUT);
  digitalWrite(FIX_LED, LOW);

  // sat number > 4
  pinMode(SATS_LED, OUTPUT);
  digitalWrite(SATS_LED, LOW);

  // update position?
  pinMode(UPDATE_BUTTON, INPUT);
  digitalWrite(UPDATE_BUTTON, HIGH); // pullup

  // updating
  pinMode(UPDATE_LED, OUTPUT);
  digitalWrite(UPDATE_LED, HIGH);

  //pinMode(DIR_PIN, OUTPUT);
  //pinMode(STEP_PIN, OUTPUT);
  stepper.setMaxSpeed(MAX_SPEED);
  stepper.setAcceleration(5000);

  // update position variables
  update = 1;

}

void loop() 
{
  // check if update needed
  if (digitalRead(UPDATE_BUTTON) == LOW)
  {
    // not updating anymore
    update = 0;
    digitalWrite(UPDATE_LED, LOW);
  }
  if (update) {
    // Get our GPS position
    // Search for GPGGA sentence
    if (Serial2.find("GPGGA,"))
    {
      // found!, read until the end
      i=0;
      byteGPS=0;
      while (byteGPS!=13) {
        if (Serial2.available() > 0)
        {
          byteGPS=Serial2.read();
          nmea_sentence[i] = byteGPS;
          i++;
        }
      }
      nmea_sentence[i-1]=0;

      // Parse the sentence for values

      // Fix
      get_nmea_value(5, fix);
      if (fix[0] == '1')
        digitalWrite(FIX_LED, HIGH);
      else
        digitalWrite(FIX_LED, LOW);

      // sat number
      get_nmea_value(6, sats);
      satn = atoi(sats);
      if (satn > 4)
        digitalWrite(SATS_LED, HIGH);
      else
        digitalWrite(SATS_LED, LOW);

      // Latitude
      get_nmea_value(1, latitude_string);
      if (latitude_string[0] != 0) 
      {
        temp_value = atof(latitude_string);
        if (temp_value != 0.0)
          latitude = nmea_to_decimal(temp_value);
      }
      //else 
      //  latitude = 9999;

      get_nmea_value(2, north_south);
      if (north_south[0] == 'S')
        latitude = -latitude;

      // Longitude 
      get_nmea_value(3, longitude_string);
      if (longitude_string[0] != 0) 
      {
        temp_value = atof(longitude_string);
        if (temp_value != 0.0)
          longitude = nmea_to_decimal(temp_value);
      }
      //else 
      //  longitude = 9999;

      get_nmea_value(4, east_west);
      if (east_west[0] == 'W')
        longitude = -longitude;


      // Altitude  
      get_nmea_value(8, altitude_string);
      if (altitude_string[0] != 0)
      {
        altitude = atof(altitude_string);
      }
      //else
      //  altitude = -9999;

    }
    //    Serial.print(latitude,5);
    //    Serial.print(",");
    //    Serial.print(longitude,5);
    //    Serial.print(",");
    //    Serial.print(altitude,2);
    //    Serial.println();
    //    Serial.println(nmea_sentence);

  }

  // Get position from xbee
  if (Serial.find("XPOS,"))
  {
    // found!, read until the end
    i=0;
    byteXBEE=0;
    while (byteXBEE!=13) {
      if (Serial.available() > 0)
      {
        byteXBEE=Serial.read();
        xbee_coordinates[i] = byteXBEE;
        i++;
      }
    }
    xbee_coordinates[i-1]=0;

    // get variables
    get_xbee_value(0, xbee_latitude_string);
    if (xbee_latitude_string[0] != 0) 
    {
      temp_value = atof(xbee_latitude_string);
      if (temp_value != 0.0)
        xbee_latitude = temp_value;
    }
    //else
    //  xbee_latitude = -9999;

    get_xbee_value(1, xbee_longitude_string);
    if (xbee_longitude_string[0] != 0) 
    {
      temp_value = atof(xbee_longitude_string);
      if (temp_value != 0)
        xbee_longitude = temp_value;
    }
    //else
    //  xbee_longitude = -9999;

    get_xbee_value(2, xbee_altitude_string);
    if (xbee_altitude_string[0] != 0) 
    {
      xbee_altitude = atof(xbee_altitude_string);
    }
    //else
    //  xbee_altitude = -9999;

  }


  // calculate degrees from East
  dy = latitude-xbee_latitude;
  dx = cos(M_PI/180.0*xbee_latitude)*(longitude-xbee_longitude);
  ang = atan2(dx,dy)*180.0/M_PI;

  // for distance, we use pythagorean theorem, as it is a small distance we don't need
  // to use trigonometry
  //distance = sqrt(((xbee_latitude-latitude)*(xbee_latitude-latitude))+((xbee_longitude-longitude)*((xbee_longitude-longitude))));


  // Debug
  delay(20);
  Serial.print(CAM_ID);
  Serial.print(",");
  Serial.print(latitude,6);
  Serial.print(",");
  Serial.print(longitude,6);
  Serial.print(",");
  Serial.println(ang);
  delay(20);

// move camera
  if (ang < 0) 
  {
    ang = 360 + ang;
  }  

  double angle_to_move = get_shortest_angle(current_angle, ang);
  
  // FIX FIX FIX DON'T LOOK HERE
  if (ang != 270.0) {
    rotate_deg_accel(angle_to_move);
    current_angle=ang;
  }

}




