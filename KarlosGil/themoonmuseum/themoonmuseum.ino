/*
 *  T H E M O O N M U S E U M 
 *  Arduino Control
 *  FabLAB Asturias, Plataforma0 Centro de produccion
 *  LABoral Centro de Arte y Creacion Industrial
 *
 */


/* 
 * COMMUNICATION PROTOCOL:
 *
 * Messages consist of 7 chars: example A1L2050
 * 
 * A: Start character (arduino)
 * 1: Arduino number (1..9)
 * L: Device - L = LAMP, R = RELAY, S = SERVO
 * 2: Device number (1..9) (if servo or relay, just ignore it as there is only one servo or relay per board)
 * X
 * X
 * X : XXX = number 0..100: LAMP = % of light, Servo or Relay, 0 = OFF, !0 = ON
 *
 */

#include <Servo.h>

#define ARDUINO_ID  2

#define LAMP_1_PIN  3
#define LAMP_2_PIN  5
#define LAMP_3_PIN  6
#define LAMP_4_PIN  11

#define RELAY_PIN    7
#define SERVO_PIN   9

#define RADIO_ON_PIN 8

#define RELAY_ON     digitalWrite(RELAY_PIN, HIGH)
#define RELAY_OFF    digitalWrite(RELAY_PIN, LOW)

#define PWM_DELAY    50

#define CURVE        -2

// Variables

Servo control_servo;
char input_buffer[8];            // incomming messages
int lamp_status[4] = {
  0,0,0,0};  // current value of each lamp

float fscale( float originalMin, float originalMax, float newBegin, float
newEnd, float inputValue, float curve){

  float OriginalRange = 0;
  float NewRange = 0;
  float zeroRefCurVal = 0;
  float normalizedCurVal = 0;
  float rangedValue = 0;
  boolean invFlag = 0;


  // condition curve parameter
  // limit range

  if (curve > 10) curve = 10;
  if (curve < -10) curve = -10;

  curve = (curve * -.1) ; // - invert and scale - this seems more intuitive - postive numbers give more weight to high end on output
  curve = pow(10, curve); // convert linear scale into lograthimic exponent for other pow function

  /*
   Serial.println(curve * 100, DEC);   // multply by 100 to preserve resolution  
   Serial.println();
   */

  // Check for out of range inputValues
  if (inputValue < originalMin) {
    inputValue = originalMin;
  }
  if (inputValue > originalMax) {
    inputValue = originalMax;
  }

  // Zero Refference the values
  OriginalRange = originalMax - originalMin;

  if (newEnd > newBegin){
    NewRange = newEnd - newBegin;
  }
  else
  {
    NewRange = newBegin - newEnd;
    invFlag = 1;
  }

  zeroRefCurVal = inputValue - originalMin;
  normalizedCurVal  =  zeroRefCurVal / OriginalRange;   // normalize to 0 - 1 float

  /*
  Serial.print(OriginalRange, DEC);  
   Serial.print("   ");  
   Serial.print(NewRange, DEC);  
   Serial.print("   ");  
   Serial.println(zeroRefCurVal, DEC);  
   Serial.println();  
   */

  // Check for originalMin > originalMax  - the math for all other cases i.e. negative numbers seems to work out fine
  if (originalMin > originalMax ) {
    return 0;
  }

  if (invFlag == 0){
    rangedValue =  (pow(normalizedCurVal, curve) * NewRange) + newBegin;

  }
  else     // invert the ranges
  {  
    rangedValue =  newBegin - (pow(normalizedCurVal, curve) * NewRange);
  }

  return rangedValue;
}


int linear_PWM(int percentage)
{
  // coefficients
  double a = 9.7758463166360387E-01;
  double b = 5.5498961535023345E-02;
  return floor((a * exp(b*percentage)+.5))-1;
}


void lamp_fade(int lamp, int value)
{
  int lamps_pins[] = { 
    LAMP_1_PIN, LAMP_2_PIN, LAMP_3_PIN, LAMP_4_PIN   };
  int i;

  if (value > lamp_status[lamp-1]) // ascending
  {
    for (i=lamp_status[lamp-1]; i<=value; i++)
    {
      analogWrite(lamps_pins[lamp-1], fscale(0, 100, 0, 255, i, CURVE));
      delay(PWM_DELAY);
    }
  }
  else
  {
    for(i=lamp_status[lamp-1]; i>=value; i--)    // descending
    {
      analogWrite(lamps_pins[lamp-1], fscale(0, 100, 0, 255, i, CURVE));
      delay(PWM_DELAY);
    }
  }

  // remember current value
  lamp_status[lamp-1] =  value;

}


int parse_message(char* msg)
{
  int value, num;

  // message starts with 'A' ?
  if (msg[0] != 'A')
    return 1;          // nope, FAIL

  // is this message for us?
  int id = msg[1] - 48;  // 48 = ASCII '0'
  if (id < 1 || id > 9)
    return 1;          // bad ID
  if (id != ARDUINO_ID)
    return 0;          // not for us

  // ok, keep parsing

  // get value from last 3 chars
  char temp[4];
  temp[0] = msg[4];
  temp[1] = msg[5];
  temp[2] = msg[6];
  temp[3] = '\0';

  value = atoi(temp);
  if (value < 0 || value > 100)    // check limits
    return 1;

  // get things done
  switch (msg[2])
  {
  case 'L':
    // get lamp number
    num =  msg[3] - 48;
    if (num < 1 || num > 4) // limits
      return 1;
    lamp_fade(num, value);
    break;

  case 'R':
    if (value == 0)
      RELAY_OFF;
    else
      RELAY_ON;
    break;

  case 'S':
    if (value == 0)
      control_servo.write(21);
    else
      control_servo.write(170);
    break;

  default:
    // bad message
    return 1;
    break;
  }    

  return 0;
}    


void setup()
{
  pinMode(LAMP_1_PIN, OUTPUT);
  pinMode(LAMP_2_PIN, OUTPUT);
  pinMode(LAMP_3_PIN, OUTPUT);
  pinMode(LAMP_4_PIN, OUTPUT);

  digitalWrite(LAMP_1_PIN, LOW);
  digitalWrite(LAMP_2_PIN, LOW);
  digitalWrite(LAMP_3_PIN, LOW);
  digitalWrite(LAMP_4_PIN, LOW);

  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, LOW);

  control_servo.attach(SERVO_PIN);
  control_servo.write(21);

  pinMode(RADIO_ON_PIN, OUTPUT);
  digitalWrite(RADIO_ON_PIN, HIGH);

  Serial1.begin(19200);

  Serial1.print("ARDUINO ");
  Serial1.print(ARDUINO_ID);
  Serial1.println(" READY");
}

void loop()
{ 

  if (Serial1.available())
  {
    // Get a message 
    if (Serial1.peek() != 'A')    // if we are not at the start of the message
      Serial1.read();           // skip it
    else
    {
      //read message (7 chars)
      Serial1.readBytes(input_buffer, 7);

      // parse message and do the stuff
      parse_message(input_buffer);
    }
  }

}


